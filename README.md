# Creating Personal Gitlab Page with Pelican

> wichit2s

Demo: https://wichit2s.gitlab.io/pelican-gitlab-page/

## 1. Apply for Gitlab account (ลงทะเบียนที่ gitlab.com)

You should have your loginname. Mine is `wichit2s`

> เมื่อสมัครแล้วจะได้ loginname ของผมชื่อ `wichit2s`

> ขั้นตอนที่เหลือทั้งหมดถ้าเจอ `wichit2s` ก็เปลี่ยนเป็น loginname ของตัวเองก็แล้วกันนะครับ

Remark: you can take a peek at your loginname by clicking on your avatar. Your loginname should be right after the @ sign.

> หมายเหตุ: ถ้าไม่รู้ว่า loginname คืออะไรให้ click ที่รูป profile ของตัวเอง ชื่อ loginname ควรจะติดกับเครื่องหมาย @

## 2. Create new project name `wichit2s.gitlab.io`

Remember to change `wichit2s` to your own loginname.

> อย่าลืมเปลี่ยน `wichit2s` เป็น loginname ของตัวเอง

## 4. Clone your project 

```sh
git config --global user.name "wichit2s"
git config --global user.email "wichits@ubu.ac.th"
git clone https://gitlab.com/wichit2s/wichit2s.gitlab.io.git
cd wichit2s.gitlab.io/
```

## 4. Install `pelican`

Open terminal and type in: 

```sh
pip3 install --user pelican
```

## 5. Initialize project

```sh
pelican-quickstart
```

## 6. Add an article `content/introduction.md`

> สร้างไฟล์สำหรับกรอก post แรกชื่อ `content/introduction.md` แล้วกรอกข้อมูลต่อไปนี้


```markdown
Title: My first post
Date: 2018-09-27 10:20
Modified: 2018-09-28 19:30
Category: Python
Tags: pelican, publishing
Slug: introduction
Authors: Wichit Sombat
Summary: introducing Wichit Sombat

This is the start of your first post. You should fill this post up.
```

## 7. Add `.gitlab-ci.yml` file

```yml
image: python:3.6-alpine

pages:
  script:
  - pip install -r requirements.txt
  - pelican content -o public -s publishconf.py
  artifacts:
    paths:
    - public/
```

## 8. Change output directory to `public`

### 8.1 change in `pelicanconf.py`

```python
PATH = 'content'
OUTPUT_PATH = 'public'
```

### 8.2 change in `develop_server.sh`

```sh
INPUTDIR=$BASEDIR/content
OUTPUTDIR=$BASEDIR/public
CONFFILE=$BASEDIR/pelicanconf.py
```
## 9. Edit `.gitignore`

```txt
/_book/
/node_modules/
/book.pdf
/book.mobi
/book.epub

*~
._*
*.lock
*.DS_Store
*.swp
*.out
*.pid
*.py[cod]
output
public
__pycache__/
```

## 10. Push all files to gitlab

```sh
git add .
git commit -m "my first commit"
git push origin master
```

## 11. Your website will be ready in less than 2 minutes.

https://[loginname].gitlab.io/

Demo: https://wichit2s.gitlab.io/pelican-gitlab-page/

